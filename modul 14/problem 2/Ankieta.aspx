﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ankieta.aspx.cs" Inherits="Ankieta" MasterPageFile="~/SzablonStrony.master" Title="Ankieta" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1"
ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    
    <div>
        <asp:AdRotator ID="AdRotator1" runat="server" AdvertisementFile="~/Adv/adv.xml" />
        <br />
        <asp:MultiView ID="mvMain" runat="server">
            <asp:View ID="vAnkietaGlowna" runat="server">Imię:<br />
                <asp:TextBox ID="tbImie" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="tbImie_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbImie" WatermarkCssClass="watermarked" WatermarkText="Podaj imię"></asp:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbImie" ErrorMessage="Pole Imię jest wymagane">*</asp:RequiredFieldValidator>
                <br />
                Płeć:<br />
                <asp:DropDownList ID="ddlPlec" runat="server">
                    <asp:ListItem Value="M">Mężczyzna</asp:ListItem>
                    <asp:ListItem Value="K">Kobieta</asp:ListItem>
                    <asp:ListItem Value="N">Nie podaje</asp:ListItem>
                </asp:DropDownList>
                <br />
                - e-mail:
                <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="tbEmail_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbEmail" WatermarkCssClass="watermarked" WatermarkText="Podaj E-Mail"></asp:TextBoxWatermarkExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbEmail" ErrorMessage="Pole E-mail jest wymagane">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbEmail" ErrorMessage=" Nieprawidłowy adres E-Mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                <br />
                - telefon:
                <asp:TextBox ID="tbTel" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="tbTel_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbTel">
                </asp:FilteredTextBoxExtender>
                <asp:TextBoxWatermarkExtender ID="tbTel_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbTel" WatermarkCssClass="watermarked" WatermarkText="Podaj telefon"></asp:TextBoxWatermarkExtender>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="tbTel" ErrorMessage="Nieprawidłowy numer telefonu podaj" ValidationExpression="([0-9]+)$">*</asp:RegularExpressionValidator>
                <br />
                - ulica:
                <asp:TextBox ID="tbUlica" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="tbUlica_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbUlica" WatermarkCssClass="watermarked" WatermarkText="Podaj ulice"></asp:TextBoxWatermarkExtender>
                <br />
                - dom:
                <asp:TextBox ID="tbDom" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="tbDom_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbDom">
                </asp:FilteredTextBoxExtender>
                <asp:TextBoxWatermarkExtender ID="tbDom_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbDom" WatermarkCssClass="watermarked" WatermarkText="Numer domu">
                </asp:TextBoxWatermarkExtender>
                <br />
                - numer mieszkania:
                <asp:TextBox ID="tbMiesz" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="tbMiesz_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbMiesz">
                </asp:FilteredTextBoxExtender>
                <asp:TextBoxWatermarkExtender ID="tbMiesz_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbMiesz" WatermarkCssClass="watermarked" WatermarkText="Numer mieszkania"></asp:TextBoxWatermarkExtender>
                <br />
                - kod pocztowy:
                <asp:TextBox ID="tbKod" runat="server"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="tbKod_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Custom, Numbers" TargetControlID="tbKod" ValidChars="-">
                </asp:FilteredTextBoxExtender>
                <asp:TextBoxWatermarkExtender ID="tbKod_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbKod" WatermarkCssClass="watermarked" WatermarkText="Kod pocztowy"></asp:TextBoxWatermarkExtender>
                <br />
                - miejscowość:
                <asp:TextBox ID="tbMiejsc" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="tbMiejsc_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbMiejsc" WatermarkCssClass="watermarked" WatermarkText="Miejscowość"></asp:TextBoxWatermarkExtender>
                <br />
                <asp:MultiView ID="mvAnkieta" runat="server">
                <asp:View ID="vNowy" runat="server">

                    Wzrost:<br />
                    <asp:TextBox ID="tbWzrost" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="tbWzrost_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbWzrost">
                    </asp:FilteredTextBoxExtender>
                    <asp:TextBoxWatermarkExtender ID="tbWzrost_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbWzrost" WatermarkCssClass="watermarked" WatermarkText="Podaj wzrost w cm">
                    </asp:TextBoxWatermarkExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="tbWzrost" ErrorMessage="Nieprawidłowy wzrost – podaj wzrost w" ValidationExpression="([0-9]+)$">*</asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="tbWzrost" ErrorMessage="Podaj poprawny wzrost w zakresie od" MaximumValue="250" MinimumValue="50" Type="Integer">*</asp:RangeValidator>
                    <br />
                    Rama roweru:<br />
                    <asp:DropDownList ID="ddlRama" runat="server">
                        <asp:ListItem Value="D">Damska</asp:ListItem>
                        <asp:ListItem Value="M">Męska</asp:ListItem>
                        <asp:ListItem Value="N">Nie ma znaczenia</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    Marka:<br />
                    <asp:CheckBoxList ID="clbMarka" runat="server">
                        <asp:ListItem Value="C">Cateye</asp:ListItem>
                        <asp:ListItem Value="G">Giant</asp:ListItem>
                        <asp:ListItem Value="K">Kenda</asp:ListItem>
                        <asp:ListItem Value="K">Kross</asp:ListItem>
                        <asp:ListItem Value="N">Nie ma znaczenia</asp:ListItem>
                    </asp:CheckBoxList>
                    <br />
                    Rodzaj roweru:<br />
                    <asp:ListBox ID="lbRodzaj" runat="server">
                        <asp:ListItem Value="G">Górski</asp:ListItem>
                        <asp:ListItem Value=" M"> Miejski</asp:ListItem>
                        <asp:ListItem Value=" S"> Szosowy</asp:ListItem>
                        <asp:ListItem Value="B">BMX</asp:ListItem>
                        <asp:ListItem Value="D">Dziecięcy</asp:ListItem>
                        <asp:ListItem Value="N">Nie wiem</asp:ListItem>
                    </asp:ListBox>
                    <br />
                    Kwota planowanych wydatków:<br />
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                        <asp:ListItem Value=" 5"> Nie ma znaczenia</asp:ListItem>
                        <asp:ListItem Value="5">do 500 zł</asp:ListItem>
                        <asp:ListItem Value="10">500 – 1000 zł</asp:ListItem>
                        <asp:ListItem Value="15">1000 - 1500 zł</asp:ListItem>
                        <asp:ListItem Value="20">pow. 1500 zł</asp:ListItem>
                        <asp:ListItem Value="1">Zakres</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    od:
                    <asp:TextBox ID="tbOd" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="tbOd_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbOd">
                    </asp:FilteredTextBoxExtender>
                    <asp:TextBoxWatermarkExtender ID="tbOd_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbOd" WatermarkCssClass="watermarked" WatermarkText="Kwota od">
                    </asp:TextBoxWatermarkExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="tbOd" ErrorMessage="Nieprawidłowa kwota – podaj" ValidationExpression="([0-9]+)$">*</asp:RegularExpressionValidator>
                    <br />
                    do:
                    <asp:TextBox ID="tbDo" runat="server"></asp:TextBox>

                    <asp:FilteredTextBoxExtender ID="tbDo_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbDo">
                    </asp:FilteredTextBoxExtender>
                    <asp:TextBoxWatermarkExtender ID="tbDo_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbDo" WatermarkCssClass="watermarked" WatermarkText="Kwota do">
                    </asp:TextBoxWatermarkExtender>

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="tbDo" ErrorMessage="Nieprawidłowa kwota – podaj" ValidationExpression="([0-9]+)$">*</asp:RegularExpressionValidator>

                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tbOd" ControlToValidate="tbDo" ErrorMessage="Wartośd w polu Do musi byd większa od" Operator="GreaterThan" Type="Integer">*</asp:CompareValidator>

                </asp:View>
                <asp:View ID="vWycieczki" runat="server">Rodzaj posiadanego roweru:<br />
                    <asp:ListBox ID="lbPosiada" runat="server">
                        <asp:ListItem Value="G">Górski</asp:ListItem>
                        <asp:ListItem Value="M">Miejski</asp:ListItem>
                        <asp:ListItem Value="S">Szosowy</asp:ListItem>
                        <asp:ListItem Value="B">BMX</asp:ListItem>
                        <asp:ListItem Value="D">Dziecięcy</asp:ListItem>
                        <asp:ListItem Value="N">Nie wiem</asp:ListItem>
                    </asp:ListBox>
                    <br />
                    Data ostatniej wycieczki:<br />
                    <asp:TextBox ID="calOstatnia" runat="server"></asp:TextBox>
                    <asp:CalendarExtender ID="calOstatnia_CalendarExtender" runat="server" Enabled="True" FirstDayOfWeek="Monday" TargetControlID="calOstatnia">
                    </asp:CalendarExtender>
                    <asp:TextBoxWatermarkExtender ID="calOstatnia_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="calOstatnia" WatermarkCssClass="watermarked" WatermarkText="Kliknij, aby podad datę ostatniej wycieczki">
                    </asp:TextBoxWatermarkExtender>
                    <br />
                    Data następnej wycieczki:<br />
                    <asp:TextBox ID="nastepnaTextBox" runat="server"></asp:TextBox>
                    <asp:CalendarExtender ID="nastepnaTextBox_CalendarExtender" runat="server" Enabled="True" FirstDayOfWeek="Monday" TargetControlID="nastepnaTextBox">
                    </asp:CalendarExtender>
                    <asp:TextBoxWatermarkExtender ID="nastepnaTextBox_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="nastepnaTextBox" WatermarkCssClass="watermarked" WatermarkText="Kliknij, aby dodać datę następnej wycieczki">
                    </asp:TextBoxWatermarkExtender>
                    <br />
                    Ilość kilometrów średnio na jednej wycieczce:<br />
                    <asp:TextBox ID="tbKm" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="tbKm_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbKm">
                    </asp:FilteredTextBoxExtender>
                    <asp:TextBoxWatermarkExtender ID="tbKm_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbKm" WatermarkCssClass="watermarked" WatermarkText="Odległość w km">
                    </asp:TextBoxWatermarkExtender>
                    <br />
                    Poziom umiejętności:<br />
                    <asp:DropDownList ID="ddlPoziom" runat="server">
                        <asp:ListItem Value="P">Początkujący</asp:ListItem>
                        <asp:ListItem Value="S">Średniozaawansowany</asp:ListItem>
                        <asp:ListItem Value="Z">Zaawansowany</asp:ListItem>
                    </asp:DropDownList>
                    </asp:View>
                    </asp:MultiView>
                Preferowany rodzaj kontaktu:
                <asp:CheckBoxList ID="cblKontakt" runat="server">
                    <asp:ListItem Value="E">E-mail</asp:ListItem>
                    <asp:ListItem Value="T">Telefon</asp:ListItem>
                    <asp:ListItem Value="O">Osobisty</asp:ListItem>
                </asp:CheckBoxList>
                <br />
                <asp:Button ID="butWyslij" runat="server" Text="Wyślij" OnClick="butWyslij_Click1" />
            </asp:View>
            <asp:View ID="vPodsumowanie" runat="server">Dziękujemy za wypełnienie ankiety:<br />
                <a href="Default.aspx"> Powrót do strony głównej</a>
            </asp:View>
        </asp:MultiView>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" />
    </div>
    </form>
    </asp:Content>
