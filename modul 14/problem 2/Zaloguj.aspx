﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="Zaloguj.aspx.cs" Inherits="Zaloguj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <span style="font-family: Calibri,Bold;font-size:11pt;color:rgb(0,0,0);font-style:normal;font-variant:normal;"><b>Zaloguj się w serwisie </b>
        <asp:Login ID="Login1" runat="server" FailureText="Logowanie nie powiodło się. Upewnij się," LoginButtonText="Zaloguj" PasswordLabelText="Hasło:" PasswordRequiredErrorMessage="Wprowadź hasło." RememberMeText="Zapamiętaj mnie" UserNameLabelText="Login:" UserNameRequiredErrorMessage="Wprowadź nazwę." TitleText="Zaloguj się">
        </asp:Login>
            <a href="Odzyskiwanie.aspx">Zapomniałem hasła</a>
        </span>
    </p>
</asp:Content>

