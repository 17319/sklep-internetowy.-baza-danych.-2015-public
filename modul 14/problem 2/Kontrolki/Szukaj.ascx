﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Szukaj.ascx.cs" Inherits="Kontrolki_Szukaj" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<p>
    Podaj nazwę produktu:
    <asp:TextBox ID="tbNazwaProduktu" runat="server"></asp:TextBox>
    <asp:TextBoxWatermarkExtender ID="tbNazwaProduktu_TextBoxWatermarkExtender" runat="server" Enabled="True" TargetControlID="tbNazwaProduktu" WatermarkCssClass="watermarked" WatermarkText="Podaj nazwę produktu">
    </asp:TextBoxWatermarkExtender>
    <asp:RequiredFieldValidator ID="rfvNazwa" runat="server" ControlToValidate="tbNazwaProduktu" ErrorMessage="Podaj fragment nazwy produktu"></asp:RequiredFieldValidator>
</p>
<p>
    <asp:Button ID="butSzukaj" runat="server" OnClick="butSzukaj_Click" Text="Szukaj" />
</p>

