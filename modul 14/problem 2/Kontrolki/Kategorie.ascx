﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Kategorie.ascx.cs" Inherits="Kontrolki_Kategorie" %>
<asp:LinqDataSource ID="ldsKategorie" runat="server" ContextTypeName="AdventureWorksDataContext" Select="new (ProductCategoryID, Name)" TableName="ProductCategories" Where="ParentProductCategoryID=null" EntityTypeName="">
</asp:LinqDataSource>
<asp:Repeater ID="repKategorie" runat="server" DataSourceID="ldsKategorie">
    
    <ItemTemplate>
        <asp:HyperLink ID="hlProdukty" runat="Server"
            NavigateUrl='<%# "~/Produkty.aspx?KatID=" + Eval("ProductCategoryID") %>'
            Text='<%# Eval("Name") %>'>
        </asp:HyperLink>
        <br />
    </ItemTemplate>



</asp:Repeater>

