﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WybraneProdukty.ascx.cs" Inherits="Kontrolki_WybraneProdukty" %>
<asp:ListView ID="lvWybraneProdukty" runat="server">
    <ItemTemplate>
        <div class="OknoProduktow">
            <asp:HyperLink ID="hlSzczegoly" runat="server" NavigateUrl='<%# "~/Szczegoly.aspx?id="+Eval("ProductID") %>' Text='<%#Eval("Name") %>' />
            <br />
            <asp:Image ID="ProduktyImage" runat="server" ImageUrl='<%# "~/Miniatura.aspx?id=" + Eval("ProductID") %>' AlternateText='<%# Eval("Name") %>' />
            <br />
            Kategoria: <%#Eval("ProductCategory.Name") %><br />
            Kolor: <%#Eval("Color") %><br />
            Cena: <%#Eval("ListPrice", "{0:C}") %><br />
        </div>
    </ItemTemplate>
</asp:ListView>

