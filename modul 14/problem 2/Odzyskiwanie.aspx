﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="Odzyskiwanie.aspx.cs" Inherits="Odzyskiwanie" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" GeneralFailureText="Twoja próba, aby uzyskać hasło nie było udane. Proszę spróbuj ponownie." QuestionFailureText="Twoja odpowiedź nie mogła być zweryfikowana. Proszę spróbuj ponownie." QuestionInstructionText="Odpowiedz na pytanie aby odzyskać hasło." QuestionLabelText="Pytanie:" QuestionTitleText="Potwierdzenie tożsamości" SubmitButtonText="Zatwierdź" SuccessText="Twoje hasło zostało wysłane do Ciebie." UserNameFailureText="Nie byliśmy w stanie uzyskać dostęp do informacji. Proszę spróbuj ponownie." UserNameInstructionText="Wpisz nazwę użytkownika, aby otrzymać hasło." UserNameLabelText="Nazwa użytkownika:" UserNameRequiredErrorMessage="Nazwa użytkownika jest wymagana" UserNameTitleText="Zapomniałeś hasła?">
    </asp:PasswordRecovery>
</asp:Content>

