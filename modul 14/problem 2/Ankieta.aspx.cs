﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ankieta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            mvMain.SetActiveView(vAnkietaGlowna);
            if (Request.QueryString["view"] == "nowy")
                mvAnkieta.SetActiveView(vNowy);
            else if (Request.QueryString["view"] == "wycieczki")
                mvAnkieta.SetActiveView(vWycieczki);
            else Response.Redirect("~/Default.aspx");
        }
    }

    protected void butWyslij_Click1(object sender, EventArgs e)
    {
        if (IsValid)
        { mvMain.SetActiveView(vPodsumowanie); }
    }
}