﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="ZmianaHasla.aspx.cs" Inherits="Zarzadzanie_ZmianaHasla" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" ChangePasswordButtonText="Zmiana hasła" ChangePasswordTitleText="Zmiana Twojego hasła" ConfirmNewPasswordLabelText="Potwierdź nowe hasło" ConfirmPasswordCompareErrorMessage="Hasła muszą być takie same." ConfirmPasswordRequiredErrorMessage="Potwierdzone hasło musi być takie samo." NewPasswordLabelText="Nowe hasło:" NewPasswordRegularExpressionErrorMessage="Podaj inne hasło." NewPasswordRequiredErrorMessage="Nowe hasło jest wymagane." PasswordLabelText="Hasło:" PasswordRequiredErrorMessage="Hasło jest wymagane" SuccessText="Twoje hasło zostało zmienione!" SuccessTitleText="Hasło zmienione." UserNameLabelText="Nazwa użytkownika:" UserNameRequiredErrorMessage="Nazwa użytkownika jest wymagana.">
    </asp:ChangePassword>
</asp:Content>

