﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Zakupy : System.Web.UI.Page
{
    Koszyk koszyk;
    protected void Page_Load(object sender, EventArgs e)
    {
        koszyk = (Koszyk)Session["koszyk"];
        if (koszyk == null)
        {
            koszyk = new Koszyk();
            Session["koszyk"] = koszyk;
        }
        if (!IsPostBack)
        {
            string req = Request.QueryString["prodID"];
            if (req != null)
            {
                int prodID = int.Parse(Request.QueryString["prodID"]);
                koszyk.Dodaj(prodID);
                //usuwanie z adresu prodID
                string url = Request.Url.AbsolutePath;
                System.Web.HttpContext.Current.RewritePath(@"~/"+ url, "", "");
            }
        }
        gvKoszyk.DataSource = koszyk.Pobierz();
        gvKoszyk.DataBind();
        kasaButton.Enabled = (koszyk.Ilosc > 0);
    }
    protected void gvKoszyk_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        koszyk.UsunElement(e.RowIndex);
        gvKoszyk.DataBind();
    }
    protected void gvKoszyk_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvKoszyk.EditIndex = e.NewEditIndex;
        gvKoszyk.DataBind();
    }
    protected void gvKoszyk_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        e.Cancel = true;
        gvKoszyk.EditIndex = -1;
        gvKoszyk.DataBind();
    }
    protected void gvKoszyk_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        TextBox tb = (TextBox)
gvKoszyk.Rows[e.RowIndex].Controls[2].Controls[0];
        try
        {
            int wartosc = int.Parse(tb.Text);
            if (wartosc < 0) { wartosc = wartosc * (-1); }
            koszyk.AktualizujIlosc(e.RowIndex, wartosc);
        }
        catch { e.Cancel = true; }
        gvKoszyk.EditIndex = -1;
        gvKoszyk.DataBind();
    }
}