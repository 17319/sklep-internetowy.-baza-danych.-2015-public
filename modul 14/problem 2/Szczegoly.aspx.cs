﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Szczegoly : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        mvSzczegoly.SetActiveView(vBledny);
        if (Request.QueryString["id"] != null)
        {
            AdventureWorksDataContext db = new AdventureWorksDataContext();
            var produkt = (from p in db.Products
                           where p.ProductID == int.Parse(Request.QueryString["id"])
                           select new
                           {
                               ProductID = p.ProductID,
                               Name = p.Name,
                               Category = p.ProductCategory.Name,
                               ListPrice = p.ListPrice,
                               Color = p.Color,
                               Description =
                               p.ProductModel.ProductModelProductDescriptions.
                               First().ProductDescription.Description
                           }).FirstOrDefault();
            if (produkt != null)
            {
                hlKoszyk.NavigateUrl = "~/Zakupy.aspx?ProdID=" + produkt.ProductID;
                mvSzczegoly.SetActiveView(vPrawidlowy);
                Page.Title = "Przeglądasz: " + produkt.Name;
                lblNazwa.Text = produkt.Name;
                imgProdukt.ImageUrl = "Miniatura.aspx?id=" + produkt.ProductID.ToString();
                lblKategoria.Text = produkt.Category;
                lblCena.Text = String.Format("{0:C}", produkt.ListPrice);
                lblKolor.Text = produkt.Color;
                lblOpis.Text = produkt.Description;

            }
        }
    }
}