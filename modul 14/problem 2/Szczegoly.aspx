﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="Szczegoly.aspx.cs" Inherits="Szczegoly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #imgProdukt {
            width: 65px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:multiview id="mvSzczegoly" runat="server">
        <asp:View ID="vPrawidlowy" runat="server">
            <asp:Label ID="lblNazwa" runat="server"></asp:Label>
            <br />
            <asp:Image ID="imgProdukt" runat="server" />
&nbsp;<br /> Kategoria:
            <asp:Label ID="lblKategoria" runat="server"></asp:Label>
            <br />
            Cena:
            <asp:Label ID="lblCena" runat="server"></asp:Label>
            <br />
            Kolor:
            <asp:Label ID="lblKolor" runat="server"></asp:Label>
            <br />
            Opis:<br />
            <asp:Label ID="lblOpis" runat="server"></asp:Label>
            <asp:HyperLink ID="hlKoszyk" runat="server"
Text="Dodaj do koszyka" /> <br />
        </asp:View>
        <asp:View ID="vBledny" runat="server">
            Nieprawidłowy produkt<br />
        </asp:View>
    </asp:multiview>
    <a href="Produkty.aspx">Powrót do przeglądania produktów</a>
</asp:Content>

