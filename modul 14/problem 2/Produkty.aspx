﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="Produkty.aspx.cs" Inherits="Produkty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">function abortPB() {
    var obj = Sys.WebForms.PageRequestManager.getInstance();
    if (obj.get_isInAsyncPostBack())
    { obj.abortPostBack(); }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p>
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        </asp:ScriptManagerProxy>
    </p>
    <asp:updatepanel id="UpdatePanel1" runat="server">
        <ContentTemplate>
            <p>
                <asp:LinqDataSource ID="ldsKategorie" runat="server" ContextTypeName="AdventureWorksDataContext" Select="new (ProductCategoryID, Name)" TableName="ProductCategories" Where="ParentProductCategoryID=null">
                </asp:LinqDataSource>
                <asp:ListBox ID="lbKategorie" runat="server" AutoPostBack="True" DataSourceID="ldsKategorie" DataTextField="Name" DataValueField="ProductCategoryID" OnSelectedIndexChanged="lbKategorie_SelectedIndexChanged"></asp:ListBox>
            </p>
            <p>
                <asp:LinqDataSource ID="ldsPodkategorie" runat="server" ContextTypeName="AdventureWorksDataContext" EntityTypeName="" Select="new (ProductCategoryID, Name)" TableName="ProductCategories" Where="ParentProductCategoryID == @ParentProductCategoryID">
                    <WhereParameters>
                        <asp:ControlParameter ControlID="lbKategorie" DefaultValue="-1" Name="ParentProductCategoryID" PropertyName="SelectedValue" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
            </p>
            <p>
                <asp:ListBox ID="lbPodkategorie" runat="server" AutoPostBack="True" DataSourceID="ldsPodkategorie" DataTextField="Name" DataValueField="ProductCategoryID"></asp:ListBox>
            </p>
            <asp:LinqDataSource ID="ldsProdukty" runat="server" ContextTypeName="AdventureWorksDataContext" EntityTypeName="" Select="new (ProductID, Name, ProductNumber, Color, ListPrice)" TableName="Products" Where="ProductCategoryID == @ProductCategoryID">
                <WhereParameters>
                    <asp:ControlParameter ControlID="lbPodkategorie" DefaultValue="-1" Name="ProductCategoryID" PropertyName="SelectedValue" Type="Int32" />
                </WhereParameters>
            </asp:LinqDataSource>
            <p>
                &nbsp;</p>
            <p>
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ldsProdukty" EnableViewState="False" PageSize="20">
                    <Columns>
                        <asp:ImageField DataAlternateTextField="Name" DataAlternateTextFormatString="Miniatura {0}" DataImageUrlField="ProductID" DataImageUrlFormatString="~/Miniatura.aspx?id={0}">
                        </asp:ImageField>
                        <asp:HyperLinkField DataNavigateUrlFields="ProductID" DataTextField="Name" HeaderText="Nazwa produktu" DataNavigateUrlFormatString="~/Szczegoly.aspx?id={0}" />
                        <asp:BoundField DataField="ProductNumber" HeaderText="Numer" ReadOnly="True" SortExpression="ProductNumber" />
                        <asp:BoundField DataField="Color" HeaderText="Kolor" ReadOnly="True" SortExpression="Color" />
                        <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Cena" ReadOnly="True" SortExpression="ListPrice" />
                        <asp:HyperLinkField DataNavigateUrlFields="ProductID" DataNavigateUrlFormatString=" ~/Zakupy.aspx?id={0}" HeaderText="Dodaj do koszyka" />
                    </Columns>
                </asp:GridView>
            </p>
        </ContentTemplate>
    </asp:updatepanel>
    <asp:updateprogress id="UpdateProgress1" runat="server">
        <ProgressTemplate>
              <a title="Paski ładowania, loadery, ajax" href="http://darmowegrafiki.5m.pl/loadery_ajax.php" ><img alt="loading_poziomy_zielony.gif" src="http://darmowegrafiki.5m.pl/loadery_ajax/gify_ikony/loading_poziomy_zielony.gif"></a>
             <br /><input type="button" id="abortButton" onclick="abortPB()" value="Anuluj aktualizację" />
        </ProgressTemplate>
    </asp:updateprogress>
    <p>
    </p>
    <p>
        &nbsp;
    </p>
</asp:Content>

