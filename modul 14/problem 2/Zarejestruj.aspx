﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="Zarejestruj.aspx.cs" Inherits="Zarejestruj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" AnswerLabelText="Odpowiedź:" AnswerRequiredErrorMessage="Odpowiedź jest wymagana." AutoGeneratePassword="True" ClientIDMode="Predictable" CompleteSuccessText="Twoje konto zostało pomyślnie utworzone." ConfirmPasswordCompareErrorMessage="Hasła muszą być identyczne." ConfirmPasswordLabelText="Potwierdź hasło:" ConfirmPasswordRequiredErrorMessage="Potwierdzenie hasła jest wymagane." CreateUserButtonText="Utwórz " DuplicateEmailErrorMessage="Adres e-mail, który podałeś jest już w użyciu. Proszę podać inny adres e-mail." DuplicateUserNameErrorMessage="Proszę podać inną nazwę." EmailRegularExpressionErrorMessage="Proszę podać inny adres e-mail." EmailRequiredErrorMessage="E-mail jest wymagany." InvalidAnswerErrorMessage="Proszę podać inną odpowiedź bezpieczeństwa." InvalidEmailErrorMessage="Proszę wpisać aktualny adres e-mail." InvalidPasswordErrorMessage="Minimalna długość hasła: {0}. Innych znaków alfanumerycznych wymagane: {1}." InvalidQuestionErrorMessage="Proszę wprowadzić inną pytanie bezpieczeństwa." PasswordLabelText="Hasło:" PasswordRegularExpressionErrorMessage="Proszę wprowadzić inne hasło." PasswordRequiredErrorMessage="Hasło jest wymagane" QuestionLabelText="Pytanie bezpieczeństwa:" QuestionRequiredErrorMessage="Pytanie bezpieczeństwa jest wymagane." UnknownErrorMessage="Twoje konto nie zostało utworzone. Proszę spróbuj ponownie." UserNameLabelText="Nazwa użytkownika:" UserNameRequiredErrorMessage="Nazwa użytkownika jest wymagana">
        <WizardSteps>
            <asp:CreateUserWizardStep runat="server" Title="Utwórz użytkownika" />
            <asp:CompleteWizardStep runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard>
</asp:Content>

