﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SzablonStrony.master" AutoEventWireup="true" CodeFile="WynikiWyszukiwania.aspx.cs" Inherits="WynikiWyszukiwania" %>

<%@ Register src="Kontrolki/Szukaj.ascx" tagname="Szukaj" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p>
        Wyniki wyszukiwania:
    </p>
    <p>
        <asp:gridview id="gvWynikiWyszukiwania" runat="server" autogeneratecolumns="False" datakeynames="ProductID">
        <Columns>
<asp:HyperLinkField DataNavigateUrlFields="ProductID"
DataNavigateUrlFormatString="~/Szczegoly.aspx?id={0}"
HeaderText="Nazwa produktu" DataTextField="Name"
Text="Nazwa produktu" />
<asp:BoundField DataField="ProductNumber"
HeaderText="Numer katalogowy" SortExpression="ProductNumber" />
<asp:BoundField DataField="Color" HeaderText="Kolor"
SortExpression="Color" />
<asp:BoundField DataField="ListPrice" DataFormatString="{0:c}"
HeaderText="Cena" SortExpression="ListPrice" />
</Columns>
        </asp:gridview>
        <br />Szukaj:<br />
    </p>
    <p>
        &nbsp;</p>
    <uc1:Szukaj ID="Szukaj1" runat="server" />
</asp:Content>

